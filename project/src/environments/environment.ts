// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url: "http://localhost/angular/slim/",
  firebase: {
    apiKey: "AIzaSyAZbQrQwBUh2r5Pd9-ZBj8w9t7xjT2SkBo",
    authDomain: "finaltest-76506.firebaseapp.com",
    databaseURL: "https://finaltest-76506.firebaseio.com",
    projectId: "finaltest-76506",
    storageBucket: "finaltest-76506.appspot.com",
    messagingSenderId: "924393318336"}
};