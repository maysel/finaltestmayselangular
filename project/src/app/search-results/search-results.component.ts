import { Component, OnInit ,Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UsersService } from "./../users/users.service";

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  constructor(private service:UsersService,private route: ActivatedRoute, private router: Router) {
    this.service = service;    
          this.route.paramMap.subscribe((params: ParamMap) => {
              this.searchValue = params.get('searchValue');
              this.service.searchProducts(this.searchValue).subscribe(response=>{
                this.products = response.json();
                this.productsKeys = Object.keys(this.products);
                this.lengthKeys = this.productsKeys.length;                           
              });      
          });
   }
searchValue;
products;
productsKeys;
lengthKeys;
  

  ngOnInit() {
  }

}
