import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersFireBaseComponent } from './users-fire-base.component';

describe('UsersFireBaseComponent', () => {
  let component: UsersFireBaseComponent;
  let fixture: ComponentFixture<UsersFireBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersFireBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersFireBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
