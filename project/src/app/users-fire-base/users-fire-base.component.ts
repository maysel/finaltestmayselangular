import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';
import { Router } from "@angular/router";

@Component({
  selector: 'users-fire-base',
  templateUrl: './users-fire-base.component.html',
  styleUrls: ['./users-fire-base.component.css']
})
export class UsersFireBaseComponent implements OnInit {
 dataFire;
  constructor(private service:UsersService, private router:Router) { }

  ngOnInit() { // פונקציה שמתעוררת ברגע שהקומפוננט נוצר, לאחר הקונסטרקטור
    this.service. getDataFire().subscribe(response=>{
    console.log(response);
    this.dataFire=response; // השמה של כל הנתונים שמגיעים מהשיטה לאובייקט בשם מסג'ס
    })

          //Login without JWT
          //var value = localStorage.getItem('auth');
          //if(value == 'true'){   
          // this.router.navigate(['/']);
          //}else{
          //this.router.navigate(['/login']);
          // }  

 }
}
