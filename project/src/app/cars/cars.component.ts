import { Component, OnInit } from '@angular/core';
import { UsersService } from './../users/users.service';

@Component({
  selector: 'cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars;
  carsKeys=[];

  constructor(private service:UsersService) { 
    service.getCars().subscribe(
      Response=>{
        this.cars = Response.json();
        this.carsKeys = Object.keys(this.cars);
      }
    )
  }

  ngOnInit() {
  }

}