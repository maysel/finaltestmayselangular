import { Component, OnInit,Output , EventEmitter } from '@angular/core';
import { UsersService } from './../users.service';
import { FormGroup , FormControl ,FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  @Output() addUser:EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;

    addform = new FormGroup({
    username:new FormControl(),
    email:new FormControl()
  });

  sendData() {
    this.addUser.emit(this.addform.value.username);
    
    console.log(this.addform.value);
    this.service.postUsers(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }
  constructor(service: UsersService,private formBuilder:FormBuilder) {
    this.service = service;
   }

   ngOnInit() {
    this.addform = this.formBuilder.group({
      username:  [null, [Validators.required]],
      email: [null, [Validators.required]],
    });
   }
 

 }
