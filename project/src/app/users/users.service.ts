import { Injectable } from '@angular/core';
import{Http, Headers} from '@angular/http'; 
import {HttpParams} from '@angular/common/http';
//import for FB
import {AngularFireDatabase} from 'angularfire2/database';
import {environment} from './../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class UsersService {

http:Http;

  getUsers(){
    return this.http.get(environment.url+'/users');
    //return ['user1','user2','user3'];
  }
//cars
  getCars(){
  return this.http.get(environment.url+'/cars');
  }

// ralvent to brings databade from DB, INSERT IN '/___' which table to bring from FB!
getDataFire(){
return this.db.list('/products').valueChanges();
}

//users
  postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.post(environment.url+'/users', params.toString(), options);
  }

   deleteUser(key){
                return this.http.delete(environment.url+ 'users/'+key);
              }
        
  putUser(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('email',data.email);
    return this.http.put(environment.url+ 'users/'+ key,params.toString(), options);
  }

  getUser(id){
     return this.http.get(environment.url+ 'users/'+ id);
  }

//login
  login(credentials){
      let options = {
        headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'
          })
      }
      let  params = new HttpParams().append('username', credentials.username).append('password',credentials.password);
      return this.http.post(environment.url+ 'login', params.toString(),options).map(response=>{ 
        let success = response.json().success;
        if (success == true){
          localStorage.setItem('auth','true');
        }else{
          localStorage.setItem('auth','false');        
        }
    });
    }


//Products

    getProducts(){
    //get messages from the SLIM rest API(DONT say DB!)
    return this.http.get(environment.url+'products');
  }

  constructor(http:Http,private db:AngularFireDatabase) { 
    this.http = http;
    
    //let service = new UsersService();
    //this.users = service.getUsers();

  }

    putProduct(data,key){
    let options = {
      headers: new Headers({
       'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.put(environment.url+'products/'+ key,params.toString(), options);
  }

  getProduct(id){
     return this.http.get(environment.url+'products/'+ id);
  }
    
   updateProduct(id,product){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',product.name).append('price',product.price);
    return this.http.put(environment.url + 'products/'+id, params.toString(), options);      

  }
  
  searchProducts(name){
    let params = new HttpParams().append('name', name);
    let options =  {
       headers:new Headers({
          'content-type':'application/x-www-form-urlencoded'     
        })
    } 
    return this.http.post(environment.url + '/products/search', params.toString(), options); 
  }
}

