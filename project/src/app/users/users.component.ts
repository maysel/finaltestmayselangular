import { Component, OnInit } from '@angular/core';
import { UsersService } from "./users.service";
import { Router } from "@angular/router";


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
usersKeys=[];
  //users = ['User1','user2','user3'];

  constructor(private service:UsersService, private router:Router) {
    service.getUsers().subscribe( 
    Response=>{//console.log(Response));
      this.users = Response.json();
      this.usersKeys = Object.keys(this.users);
              } 
   );
}

  optimisticAdd(user){
    var newKey = parseInt(this.usersKeys[this.usersKeys.length - 1],0) + 1;
    var newUserObject = {};
    newUserObject['username'] = user; 
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
  }

  pessimisticAdd(){
    this.service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
   })
  }

  deleteUser(key){ //מימוש אופטימיסטיק דליט
  console.log(key);
  let index = this.usersKeys.indexOf(key); //מציאת המקום של הקי שנשלח בפונקציה
  this.usersKeys.splice(index,1); // מחיקת הערך מהמערך


//delete from server
  this.service.deleteUser(key).subscribe(
    response=>console.log(response) // לצורך בדיקה
  );
}

  updateUser(id){
    this.service.getUser(id).subscribe(response=>{
        this.users = response.json();      
    })
  }

  logout(){ 
      localStorage.removeItem('auth');      

      this.router.navigate(['/login']);
  }



  ngOnInit() {
  }

}
