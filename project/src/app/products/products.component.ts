//import { Component, OnInit } from '@angular/core';
//import { ProductsService } from "./products.service";
import { UsersService } from "../users/users.service";
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products;//תכונה ריקה בשביל שנוכל להשתמש בה בקונסטרקטור
  productsKeys=[];

lengthKey;
product_service:UsersService;

  searchForm = new FormGroup({
  name:new FormControl()
  }); 
 
    sendData(){
    this.router.navigate(['/search-results/' + this.searchForm.value.name]);
 }

  constructor(private service:UsersService,private router:Router, private formBuilder:FormBuilder)
   {
     this.product_service = service;
    service.getProducts().subscribe(
    response=>{//console.log(response)//arrow function .json() converts the string that we recived to jason
    this.products= response.json();           
    this.productsKeys = Object.keys(this.products);
   }
  
    )};

updateProduct(id){
 this.service.getProduct(id).subscribe(response=>{
 this.products = response.json();      
 })
}


  ngOnInit() {
  }

}