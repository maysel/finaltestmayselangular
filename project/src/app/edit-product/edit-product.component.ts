import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../users/users.service';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
      @Output() editProduct:EventEmitter<any> = new EventEmitter<any>(); 
      @Output() editProductPs:EventEmitter<any> = new EventEmitter<any>();

      service:UsersService;
      name;
      price;
      product;

      updateform = new FormGroup({
      name:new FormControl(),
      price:new FormControl()
      });


  constructor(private route: ActivatedRoute ,service: UsersService, private formBuilder: FormBuilder, private router: Router) {

    this.service = service;
   }

   sendData() {
          this.editProduct.emit(this.updateform.value.name);
          console.log(this.updateform.value);

          this.route.paramMap.subscribe(params=>{
            let id = params.get('id');
            this.service.putProduct(this.updateform.value, id).subscribe(
              response => {
                console.log(response.json());
                this.editProductPs.emit();
                this.router.navigate(['/']);
              }
            );
          })
        }
        user;

ngOnInit() {
this.route.paramMap.subscribe(params=>{
  let id = params.get('id');
  console.log(id);
  this.service.getProduct(id).subscribe(response=>{
    this.product = response.json();

    console.log(this.product);

    this.name = this.product.name
    this.price = this.product.price  
  })
})
}
}
