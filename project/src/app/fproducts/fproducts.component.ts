import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users/users.service";

@Component({
  selector: 'app-fproducts',
  templateUrl: './fproducts.component.html',
  styleUrls: ['./fproducts.component.css']
})
export class FproductsComponent implements OnInit {

  dataFire;

  constructor(private service:UsersService) { }

  ngOnInit() { // פונקציה שמתעוררת ברגע שהקומפוננט נוצר, לאחר הקונסטרקטור
       this.service. getDataFire().subscribe(response=>{
         console.log(response);
       this.dataFire=response; // השמה של כל הנתונים שמגיעים מהשיטה לאובייקט בשם מסג'ס
  })
}

}
