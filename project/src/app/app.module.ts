import {RouterModule} from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from "./users/users.service";

import { HttpModule } from "@angular/http";
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CarsComponent } from './cars/cars.component';

//imports for FB
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from './../environments/environment';
import { UsersFireBaseComponent } from './users-fire-base/users-fire-base.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { UserUpdateComponent } from './users/user-update/user-update.component';
import { ProductsComponent } from './products/products.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { FproductsComponent } from './fproducts/fproducts.component';
import { SearchResultsComponent } from './search-results/search-results.component';




@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    UsersComponent,
    NotFoundComponent,
    LoginComponent,
    WelcomeComponent,
    CarsComponent,
    UsersFireBaseComponent,
    UserCreateComponent,
    UserUpdateComponent,
    ProductsComponent,
    EditProductComponent,
    FproductsComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([ // בניה של הראוטס
      {path:'',component:ProductsComponent}, // הראוט הראשון זה דף הבית לוקלהוסט:4200
      {path:'users', component:UsersComponent},
      {path:'products', component:ProductsComponent},
      {path: 'edit-product/:id', component: EditProductComponent},
      {path: 'search-results/:searchValue',component: SearchResultsComponent  },
      {path:'nameFire',component:FproductsComponent},
      //{pathMatch:'full',path:'user-create/:id',component:UserCreateComponent},
      {path:'login',component:LoginComponent},
      {path:'user-update/:id',component:UserUpdateComponent},
      {path:'cars',component:CarsComponent},
      {path:'usersFireBase',component:UsersFireBaseComponent},
      {path:'**',component:NotFoundComponent}// צריך להופיע אחרון כי הוא תופס את כל מה שלא מוגדר ומעביר אליו

    ])
  ],
  providers: [
    UsersService,
    //ProductsService,
    HttpModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
